# ProjectEuler
My take on mathematical problems listed on Project Euler website (https://projecteuler.net/about)

[1. Multiples of 3 and 5 documentation](https://gitlab.com/pflajszer/ProjectEuler/-/tree/main/1.%20Multiples%20of%203%20and%205)  
[2. Even Fibonacci numbers](https://gitlab.com/pflajszer/ProjectEuler/-/tree/main/2.%20Even%20Fibonacci%20numbers)
