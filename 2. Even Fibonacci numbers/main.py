
def even_fibonacci_sequence_terms_sum(maxVal):
    fibSeq = [1, 2]
    nxt = 0
    prev = fibSeq[1]
    prev_prev = fibSeq[0]

    while nxt < maxVal:
        nxt = prev + prev_prev
        prev_prev = prev
        prev = nxt
        if nxt % 2 == 0:
            fibSeq.append(nxt)
        else:
            continue
    result = sum(fibSeq[1:])
    return result


if __name__ == '__main__':
    maxVal = 4_000_000
    result = even_fibonacci_sequence_terms_sum(maxVal)
    print(result)