import math
import sys

def convert_args_to_list_of_int(args):
    l = len(args)
    inputStrings = args[1:l-1].split(',')
    inputs = list(map(int, inputStrings))
    return inputs


def find_number_of_instances_in_target(input, target):
    if(input <= 0 or target <= 0):
        return -1
    # ensure we're looking for numbers below the target
    target = target - 1
    # calculate instance for input number in the target
    exactInstanceCount = target / input
    # make sure it's a natural number
    naturalNumberInstanceCount = math.floor(exactInstanceCount)

    return naturalNumberInstanceCount



def sum_multiplications(input, numOfInstances):
    # generate a range so we can iterate on it
    ourRange = range(1, numOfInstances + 1)
    results = []
    # multiply each index by an input and save results
    for i in ourRange:
        results.append(i * input)
    ourSum = sum(results)
    return ourSum

def list_all_natural_numbers_below_target_that_are_multiples_of_inputs(inputs, target):
    results = []
    for i in inputs:
        numOfInstancesForInput = find_number_of_instances_in_target(i, target)
        sumOfMult = sum_multiplications(i, numOfInstancesForInput)
        results.append(sumOfMult)

    result = sum(results)
    return result



if __name__ == "__main__":
    # hard-coded args:

    #target = 1000
    #inputs = [3, 5]
    
    # args from console:
    
    # read target
    target = int(sys.argv[1])
    # read inputs
    inputs = convert_args_to_list_of_int(sys.argv[2])

    result = list_all_natural_numbers_below_target_that_are_multiples_of_inputs(inputs, target)

    print(f'The solution to the problem for target {target} and inputs {inputs} is {result}')
