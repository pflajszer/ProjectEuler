if __name__ == "__main__":

    # constants:
    target = 1000
    input1 = 3
    input2 = 5

    # range of numbers between 0 and a target number that are divisable by input1 value:
    range_1 = range(0, target, input1)
    # range of numbers between 0 and a target number that are divisable by input2 value:
    range_2 = range(0, target, input2)
    # convert the first range to a set (in theory, it is a set already as no duplicate values are present):
    set_1 = set(range_1)
    # add second range of numbers to the set. there will be some duplicates in it (like 3 * 10 = 30, but also 5 * 6 = 30), but because those are sets, they get deduped:
    set_1.update(range_2)
    # finally, sum up both sets:
    result = sum(set_1)

    print(f'The solution to the problem for target {target} and inputs {input1} and {input2} is {result}')
