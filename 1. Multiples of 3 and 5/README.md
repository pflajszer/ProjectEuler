- [1. Multiples of 3 and 5](#1-multiples-of-3-and-5)
  - [1.1. Problem](#11-problem)
  - [1.2. Solutions:](#12-solutions)
    - [1.2.1. Correct solution: Deduped sets](#121-correct-solution-deduped-sets)
    - [1.2.2. Wrong solution: Doesn't take care of duplications](#122-wrong-solution-doesnt-take-care-of-duplications)
  - [1.3. Code](#13-code)
    - [1.3.1. Tech](#131-tech)
    - [1.3.2. Requirements](#132-requirements)
    - [1.3.3. Usage](#133-usage)


# 1. Multiples of 3 and 5

The original problem on the Project Eulers page can be found <a href="https://projecteuler.net/problem=1" target="_blank">here</a>.

## 1.1. Problem

If we list all the natural numbers below $`10`$ that are multiples of $`3`$ or $`5`$, we get $`3`$, $`5`$, $`6`$ and $`9`$. The sum of these multiples is $`23`$.

Find the sum of all the multiples of $`3`$ or $`5`$ below $`1000`$.

## 1.2. Solutions: 

### 1.2.1. Correct solution: Deduped sets

**Problem Decomposition**

1. Determine natural numbers between $`1`$ and $`999`$ that are divisable by $`3`$
2. Determine natural numbers between $`1`$ and $`999`$ that are divisable by $`5`$ and are not in the other set
3. Sum both sets

**Mathematical notation**

1. Using set theory, we can elegantly create a set of natural numbers larger than $`0`$, such that is smaller than $`1000`$ and divisable by $`3`$:
```math
A = \{ n \in 3\mathbb{N}_{1} | n < 1000 \}
```
2. Similarly, we can create set $`B`$ and prevent it from having duplicates by using $`\notin`$ (not in) operator:

```math
B = \{ n \in 5\mathbb{N}_{1} | n < 1000 \land n \notin A \}
```

3. Finally, sum both of the sets together:
```math
result = \sum_{i \in A \cup B } i
```

```math
result = 233168
```

### 1.2.2. Wrong solution: Doesn't take care of duplications

**The below solution doesn't work correctly, unless we don't care about the numbers that are both a duplication of 3 and 5 (i.e. $`30`$ is $`3 * 10`$ and also $`5 * 6`$). This problem implies that we don't want those number to be summed up twice, which I haven't thought of at a time of writing this.**

**Problem decomposition**

1. Determine the number of instances of a number ($`3`$ or $`5`$) in the target number ($`1000`$) to know how many numbers we have to sum up (from the above example, there are $`3`$ instances of multiples of $`3`$ in $`10`$. Those are: $`3`$, $`6`$ and $`9`$)
2. Sum up all the numbers within a range from $`1`$ to $`n-1`$ (since we're interested in natural numbers **below** $`n`$), where the `index % k == 0` where $`n`$ is a $`1000`$ and $`k`$ is $`3`$ or $`5`$ (from the above example, $` 3 + 6 + 9 = 18 `$)
3. Perform the above calculations for both $`3`$ and $`5`$ as an input
4. Sum the results for both $`3`$ and $`5`$

**Mathematical notation**

1. The way we can figure out the number of instances of a natural number in another number (i.e. number of $`3`$ in a $`10`$) is done by division.
We have to floor the result, so we get a natural number too. We also have to remember that we're interested in the number below $`10`$, so we can simply assume $`10 - 1`$ .
The resulting notation would be: $` \lfloor\frac{10-1}{3}\rfloor = 3 `$
2. Sticking to the example of $`3`$ in $`10`$, summing up all the numbers in the range would have the following notation: $` \sum_{i = 1} ^{\lfloor\frac{10-1}{3}\rfloor} 3i `$ .
Here, we simply take all the natural numbers from $`1`$ to the sum from the previous step, multiply each number in range by the input number (in our case it's $`3`$) and sum them all up. The resulting equation would translate to: $` (1 * 3) + (2 * 3) + (3 * 3) = 18 `$ .
Looking at this, we can simply parameterize the equation to look something like:


```math
 \sum_{i = 1} ^{\lfloor \frac{n-1}{k} \rfloor} ki
 ```

Where:
- $`n`$ is a target number. For our problem it's a $`1000`$
- $`k`$ is an input number. For our problem this will be $`3`$ and $`5`$
- $`i`$ is obviously just an index


3. Because we determined the above, we can now perform calculations like:

```math
x = \sum_{i = 1} ^{\lfloor{\frac{n-1}{3}}\rfloor} 3i 
```

```math
x = 166833
```

```math
y = \sum_{i = 1} ^{\lfloor{\frac{n-1}{5}}\rfloor} 5i
```

```math
y = 99500
```

4. This is self explanatory, I hope.

```math
result = 166833 + 99500
```

```math
result = 266333
```

The full equation looks like this:

```math
\sum_{i = 1} ^{\lfloor{\frac{n-1}{3}}\rfloor} 3i + 
\sum_{i = 1} ^{\lfloor{\frac{n-1}{5}}\rfloor} 5i
= 266333
```

## 1.3. Code

### 1.3.1. Tech

The code is written with `Python 3.8.7`.

### 1.3.2. Requirements

None

### 1.3.3. Usage

**Correct solution:**

The below solution can be run directly in VS Code or via command-line. The arguments are hard-coded. The script is very simple using ranges and sets. You can change `input1`, `input2` and `target` variables.

```
python main.py
```


**Wrong solution:**

```
python main_wrong.py target [inputs]
```

Example
```
python main.py 1000 [3,5]
```

_Note: you can also run directly in VS Code. To do so, uncomment `hard-coded args` and comment out `args from console` section._
